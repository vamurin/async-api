import * as serverApi from './db';

function parseAndReturnAnswer(response) {
    let info = JSON.parse(response);

    if (info.code === 200) {
        return info.data;
    }
    else {
        throw new Error(info.status);
    }
}

function throwError(msg) {
    console.log(msg);
    return new Promise(
        function (resolve, reject)
        {
            reject(msg);
        });
}

async function all() {
    try {
        let response = await serverApi.all();
        return parseAndReturnAnswer(response);
    }
    catch (e) {
        return throwError(`Error in function Articles->All, Msg: ${e}`);
    }
}

async function one(id) {
    try {
        let response = await serverApi.get(id);
        return parseAndReturnAnswer(response);
    }
    catch (e) {
        return throwError(`Error in function Articles->One(id: ${id}), Msg: ${e}`);
    }
}

async function remove(id) {
    try {
        let response = await serverApi.remove(id);
        return parseAndReturnAnswer(response);
    }
    catch (e) {
        return throwError(`Error in function Articles->Remove(id: ${id}), Msg: ${e}`);
    }
}

export {all, one, remove};