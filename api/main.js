import * as ArticlesModel from './articles';

async function main()
{
    let articles;
    // получаем список всех статей
    try {
        articles = await ArticlesModel.all();
    }
    catch (e) {
        console.log('Could not get articles, exiting');
        return;
    }

    console.log('articles count = ' + articles.length);

    // берём случайный индекс
    let ind = Math.floor(Math.random() * articles.length);
    console.log('select index ' + ind + ', id = ' + articles[ind].id)

    // получаем статью по id
    let article;
    try {
        article = await ArticlesModel.one(articles[ind].id);
    }
    catch (e) {
        console.log('Could not get article, exiting');
        return;
    }
    console.log(article);

    // пробуем удалить её
    let res;
    try {
        res = await ArticlesModel.remove(article.id);
    }
    catch (e) {
        console.log('Could not remove article, exiting');
        return;
    }
    console.log('что с удалением? - ' + res);

    // а сколько статей в базе сейчас
    try {
        articles = await ArticlesModel.all();
    }
    catch (e) {
        console.log('Could not get articles again, exiting');
        return;
    }
    console.log('articles count = ' + articles.length);

    // еще раз удаляем то же самое
    try {
        res = await ArticlesModel.remove(article.id);
    }
    catch (e) {
        console.log('Could not remove article, exiting');
        return;
    }
    console.log('что с удалением? - ' + res);
}

main();